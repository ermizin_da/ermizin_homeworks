class Program2 {
    public static int getIndex(int[] array, int x) {

        for (int i = 0; i < array.length ; i++) {
        	if (array[i] == x) {
        		return i;
        	}
       	}
        return -1;
    }

    public static void print(int[] array) {
        for (int i = 0; i < array.length; i++) {
        	for (int j = i; j < array.length - 1; j++) {
        	 	if (array[j] == 0) {
               		array[j] = array[j + 1];
                	array[j + 1] = 0;
            	}
            }

        	System.out.print(array[i] + " ");
        }
    
	}

    public static void main(String[] args) {
        int [] a = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        int x = 14;
        System.out.println(getIndex(a, x));
        print(a);
    }
}
